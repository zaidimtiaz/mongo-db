> db.inventory.find({"qty": {"$lt": 100}})
{ "_id" : ObjectId("5dcff69af466bd05485dc587"), "item" : "journal", "qty" : 25, "tags" : [ "blank", "red" ], "dim_cm" : [ 14, 21 ] }
{ "_id" : ObjectId("5dcff69af466bd05485dc588"), "item" : "notebook", "qty" : 50, "tags" : [ "red", "blank" ], "dim_cm" : [ 14, 21 ] }
{ "_id" : ObjectId("5dcff69af466bd05485dc58a"), "item" : "planner", "qty" : 75, "tags" : [ "blank", "red" ], "dim_cm" : [ 22.85, 30 ] }
{ "_id" : ObjectId("5dcff69af466bd05485dc58b"), "item" : "postcard", "qty" : 45, "tags" : [ "blue" ], "dim_cm" : [ 10, 15.25 ] }


> db.inventory.find({"qty": {"$gt": 99}})
{ "_id" : ObjectId("5dcff69af466bd05485dc589"), "item" : "paper", "qty" : 100, "tags" : [ "red", "blank", "plain" ], "dim_cm" : [ 14, 21 ] }