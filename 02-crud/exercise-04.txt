> db.feed.find()
{ "_id" : ObjectId("5dd128aec16d5e9832b2df8f"), "user_name" : "alec123", "user_id" : 3, "timestamp" : 1557491400, "rank" : 3.14, "public" : true, "message" : "Going on an awesome trip #travel #hiking", "tags" : [ "travel", "hiking" ] }
{ "_id" : ObjectId("5dd128c3c16d5e9832b2df90"), "user_name" : "hollie999", "user_id" : 17, "timestamp" : 1557491401, "rank" : 5, "public" : true, "message" : "What a weather! #rain #sad", "tags" : [ "rain", "sad" ] }


> db.feed.find().pretty()
{
        "_id" : ObjectId("5dd128aec16d5e9832b2df8f"),
        "user_name" : "alec123",
        "user_id" : 3,
        "timestamp" : 1557491400,
        "rank" : 3.14,
        "public" : true,
        "message" : "Going on an awesome trip #travel #hiking",
        "tags" : [
                "travel",
                "hiking"
        ]
}
{
        "_id" : ObjectId("5dd128c3c16d5e9832b2df90"),
        "user_name" : "hollie999",
        "user_id" : 17,
        "timestamp" : 1557491401,
        "rank" : 5,
        "public" : true,
        "message" : "What a weather! #rain #sad",
        "tags" : [
                "rain",
                "sad"
        ]
}

> db.feed.find({"user_id": 17})
{ "_id" : ObjectId("5dd128c3c16d5e9832b2df90"), "user_name" : "hollie999", "user_id" : 17, "timestamp" : 1557491401, "rank" : 5, "public" : true, "message" : "What a weather! #rain #sad", "tags" : [ "rain", "sad" ] }

> db.inventory.find({"item": "paper"})
{ "_id" : ObjectId("5dcff69af466bd05485dc589"), "item" : "paper", "qty" : 101, "tags" : [ "red", "blank", "plain" ], "dim_cm" : [ 14, 21 ], "available" : true }
{ "_id" : ObjectId("5dd129f0c16d5e9832b2df95"), "item" : "paper", "qty" : 100, "tags" : [ "red", "blank", "plain" ], "dim_cm" : [ 14, 21 ] }