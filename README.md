# NoSQL Databases

In this modules, students will learn about NoSQL databases, based on example of 
MongoDB. After completing this module, they should feel comfortable starting
a MongoDB server and performing CRUD operations. 

Topics covered in this module:
1. NoSQL database definition
2. CAP theorem*
3. Installing and running MongoDB 
4. MongoDB structure
5. BSON objects
6. CRUD

## Presentation 
Presentation can be found [here](https://gitlab.com/sda-international/program/common/nosql-databases/wikis/home)

## Further reading
- https://en.wikipedia.org/wiki/NoSQL
- https://docs.mongodb.com/manual/crud/
- https://docs.mongodb.com/manual/reference/operator/query/#query-selectors
