> db.feed.insertOne(
... {
...     "user_name": "alec123",
...     "user_id": 3,
...     "timestamp": 1557491400,
...     "rank": 3.14,
...     "public": true,
...     "message": "Going on an awesome trip #travel #hiking",
...     "tags": [
...         "travel",
...         "hiking"
...     ]
... }
... )

{
        "acknowledged" : true,
        "insertedId" : ObjectId("5dd128aec16d5e9832b2df8f")
}


> db.feed.insertOne(
...
... {
...     "user_name": "hollie999",
...     "user_id": 17,
...     "timestamp": 1557491401,
...     "rank": 5.00,
...     "public": true,
...     "message": "What a weather! #rain #sad",
...     "tags": [
...         "rain",
...         "sad"
...     ]
... }
... )

{
        "acknowledged" : true,
        "insertedId" : ObjectId("5dd128c3c16d5e9832b2df90")
}


> db.feed.find()
{ "_id" : ObjectId("5dd128aec16d5e9832b2df8f"), "user_name" : "alec123", "user_id" : 3, "timestamp" : 1557491400, "rank" : 3.14, "public" : true, "message" : "Going on an awesome trip #trave
l #hiking", "tags" : [ "travel", "hiking" ] }
{ "_id" : ObjectId("5dd128c3c16d5e9832b2df90"), "user_name" : "hollie999", "user_id" : 17, "timestamp" : 1557491401, "rank" : 5, "public" : true, "message" : "What a weather! #rain #sad", "ta
gs" : [ "rain", "sad" ] }


> show collections
feed
patients
personal


> show dbs
admin       0.000GB
config      0.000GB
example_db  0.000GB
local       0.000GB